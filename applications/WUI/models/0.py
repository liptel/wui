# coding: utf8
import logging, logging.handlers

def get_configured_logger(name):
    logger = logging.getLogger(name)
    if (len(logger.handlers) == 0):
        # This logger has no handlers, so we can assume it hasn't yet been configured
        # (Configure logger)
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('applications/provision/static/provision.log')
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s \t%(levelname)s \t%(message)s')
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(fh)

    return logger

# Assign application logger to a global var  
logger = get_configured_logger(request.application)

sysconfdb = DAL('sqlite://sysconf.sqlite')
sysconfdb.define_table('configuration',
    Field('key', 'string', requires = [IS_NOT_EMPTY(), IS_NOT_IN_DB(sysconfdb, 'configuration.key'), IS_IN_SET(['AstDBuser', 'AstDBpass', 'AstDBhost', 'AstDBbase', 'ExportPath'], zero=T('choose one'))]), 
    Field('value', 'string', requires = IS_NOT_EMPTY()))
    
sysconfdb.define_table('adfields',
    Field('key', 'string', requires = [IS_NOT_EMPTY(), IS_NOT_IN_DB(sysconfdb, 'adfields.key'), IS_MATCH('^P\d+$', error_message='Must be PX, ex. P1 or P152')]), 
    Field('value', 'string', requires = IS_NOT_EMPTY()))


try:
    dbuser = sysconfdb(sysconfdb.configuration.key == 'AstDBuser').select().first().value
except:
    dbuser= None
    logger.error('Username for AstDB access not in DB.')

try:
    dbpass = sysconfdb(sysconfdb.configuration.key == 'AstDBpass').select().first().value
except:
    dbpass= None
    logger.error('Password for AstDB access not in DB.')

try:
    dbhost = sysconfdb(sysconfdb.configuration.key == 'AstDBhost').select().first().value
except:
    dbhost= None
    logger.error('Host for AstDB access not in DB.')
   
try:
    dbbase = sysconfdb(sysconfdb.configuration.key == 'AstDBbase').select().first().value
except:
    dbbase= None
    logger.error('Database name for AstDB access not in DB.')
