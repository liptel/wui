# coding: utf8
err_astdb_conn = None
if dbuser and dbpass and dbhost and dbbase:
    try:
        astdb = DAL('mysql://%s:%s@%s/%s' % (dbuser, dbpass, dbhost, dbbase), migrate_enabled=False, pool_size=10)
        astdb.define_table('sip',
            Field('id', 'string', length=20),
            Field('keyword', 'string', length=30),
            Field('data', 'string', length=255),
            Field('flags', 'integer')
        )
        astdb.define_table('devices',
            Field('id', 'string', length=20),
            Field('tech', 'string', length=10),
            Field('dial', 'string', length=50),
            Field('devicetype', 'string', length=5),
            Field('user', 'string', length=50),
            Field('description', 'string', length=50),
            Field('emergency_cid', 'string', length=100)
        )
    except:
        err_astdb_conn="Error connecting to MySQL database. Possible reason: wrong privileges, mysqld down, network error, wrong parameters."
else:
    err_astdb_conn="Error connecting to MySQL database. At least one parameter missing. Run System Config."


accdb = DAL('sqlite://accdb.sqlite')
accdb.define_table('accounts',
    Field('macaddr', 'string', requires = [IS_NOT_EMPTY(), IS_NOT_IN_DB(accdb, 'accounts.macaddr'), 
            IS_MATCH('[0-9abcdef]{12}', error_message='Not a MAC address (format: abcde01234567)')]),
    Field('account', 'string', requires = IS_NOT_EMPTY()))
