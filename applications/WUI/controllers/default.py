# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
import applications.provision.modules.functions as fn

def index():
    if err_astdb_conn:
        response.flash = err_astdb_conn
    return dict()
    
def devdb():
    grid = SQLFORM.grid(
        accdb.accounts, 
        links =[lambda row: A(SPAN(_class="icon search icon-search"), SPAN('Details', _class="buttontext button"), _class="w2p_trap button btn",_href=URL("provision", "default", "details",args=[row.id]))], 
        selectable = lambda ids : redirect(URL('default', 'generate', vars=dict(id=ids))), 
        details = False, 
        deletable= True, 
        user_signature = False, 
        paginate=100
        )
    return locals()

def generate():
    if not dbhost:
        redirect(URL('index'))
    if not request.vars:
        rvars_rows = accdb(accdb.accounts.id > 0).select(accdb.accounts.id)
        rvars = { 'id': [ str(k.id) for k in rvars_rows] }
        print 'brum', rvars
    else:
        rvars = dict(request.vars)                                 #ex. {id:[1,2,3]}
        if type(rvars['id']) is not list:
            rvars = { 'id': [rvars['id']]}
        
    fn.prepare_data_and_log(astdb, accdb, sysconfdb, dbhost, rvars['id'], logger)                # elastix db is on same server as the SIP
    session.flash = 'Process complete. Please read the log.'
    redirect(URL('rfile'))
    return
    
def rfile():
    with open('applications/provision/static/provision.log', 'r') as f:
        content = f.readlines()
    return dict(content=content)
    
def details():
    accid = request.args(0)
    if accid:
        acc  = accdb(accdb.accounts.id == accid).select().first().account
        grid = SQLFORM.grid(astdb.sip.id == acc, details = False, editable = False, deletable = False, user_signature=False, paginate = 100, create = False, csv = False)
        devdesc = astdb(astdb.devices.id == acc).select().first().description
        return dict(grid=grid, devdesc=devdesc)
    else:
        session.flash = 'No device ID given.'
        redirect(URL('index'))


def import_csv(table, f): 
    table.import_from_csv_file(f)
    redirect(URL('devdb'))

def choose_csv():
    if request.vars.csvfile != None:
        if type(request.vars.csvfile) is not str: 
            import_csv(accdb['accounts'], request.vars.csvfile.file)
        else:
            session.flash = 'No CSV file selected.'
            redirect(URL('choose_csv'))
    return dict()

def dellog():
    with open('applications/provision/static/provision.log', 'w') as f: pass
    session.flash = 'Log file deleted.'
    redirect(URL('rfile'))
    
def dellall():
    accdb.accounts.truncate()
    session.flash = 'Database emptied.'
    redirect(URL('devdb'))
    
def sysconf():
    form = SQLFORM.grid(sysconfdb.configuration, create = True, user_signature=False, deletable = True, csv=False)
    return dict(form=form)
    
def addfields():
    form = SQLFORM.grid(sysconfdb.adfields, create = True, user_signature=False, deletable = True, csv=False)
    return dict(form=form)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in 
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
