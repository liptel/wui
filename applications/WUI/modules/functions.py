#!/usr/bin/env python
# coding: utf8
from gluon import *
import os
import subprocess

###################################################################
# Args to look for in astdb
args = (
    'secret',
    #'disallow', pro telefon nenma vyznam
    'allow',
    'dtmfmode',
    #'port',
    #'callerid'  
    )
###################################################################

###################################################################
# Codecs translation dictionary
codecs_dict = {
    'alaw': '8',
    'ulaw': '0',
    'gsm' : '3',
    'g726': '2',
    'g729': '18',
    'g722': '9'
}

###################################################################

###################################################################
# dtmf translation dictionary
dtmf_dict = {
    'rfc2833': '1',
    'inband':  '8',
    'info':    '2'
}

###################################################################

###################################################################
default_codec_tuple = (
                    codecs_dict['alaw'],
                    codecs_dict['ulaw'],
                    codecs_dict['gsm'],
                    codecs_dict['g726'],
                    codecs_dict['g729']
                    )
###################################################################


def get_astdb_record(db, id, keyword):
    qry1  = db.sip.id==id                    # select only where given id
    qry2  = db.sip.keyword==keyword          # select only where given keyword
    arg   = db.sip.data                      # select what (what to return)
    rows  = db(qry1 & qry2).select(arg)      # every select returns rows object
    row   = rows.first()                     # always only single row in rows, therefore select it using first
    value = row.data                         # read value of given column
    return  value
    
def insert(dbuser, dbpass, dbhost, dbbase):
    istring="""
# coding: utf8
dbuser = '%s'
dbpass = '%s'
dbhost = '%s'
dbbase = '%s'
    """ % (dbuser, dbpass, dbhost, dbbase)
    with open('applications/provision/models/0.py', 'w') as f:
        f.write(istring)
        print 'brum'
    return

def modify_syntax(switch=None, arg1=None):
    import re
    
    if not switch: return 0
    else:
        if switch == 'codec':
            allow  = arg1
            if allow == '' or allow == 'all': # what to do if no codec is specified
                codec_tuple = default_codec_tuple
            else:
                specified_codecs = allow.split('&')

                try: # what if a codec is not in dictionary codecs_dict
                    codec_list = [ codecs_dict[k] for k in specified_codecs ]
                    if len(specified_codecs) < 5:
                        codec_list = codec_list + list(default_codec_tuple) # add first n default codecs to match the number 5
                    codec_tuple = tuple(codec_list[:5])
                except:
                    codec_tuple = default_codec_tuple
            return codec_tuple # ('0', '8', ...)
        # Device name (caller id) is taken from different database.    
        #elif switch == 'cid':
        #    callerid = arg1
        #    callerid_name, callerid_num = re.findall('([A-Za-z0-9 ]+) \<(\d+)\>', callerid)[0] # ex. John Doe 12536 <12536>
        #    if callerid_name == 'device': callerid_name = callerid_num
        #    return callerid_name
            
                                    
def prepare_data_and_log(astdb, accdb, adddb, host, ids, log):
    import json
    ###################################################################
    # do logic for each entry in ids
    for lid in ids:
        log.debug('Getting info on entry with ID=%s.' % (lid))
        row = accdb(accdb.accounts.id == lid).select(accdb.accounts.ALL).first()
        account, macaddr = row.account, row.macaddr
        log.debug('ID=%s has MAC address of %s, and belongs to account %s.' % (lid, macaddr, account))
        
        if macaddr[:6] != '000b82':        # if not a GS phone, skip it.
            log.warning('Device with ID=%s is not a Grandstream phone, SKIPPING.' % (lid))
            continue
        
        try: # try to read info about the extension
            argdict = {}
            for k in args:
                argdict[k] = astdb((astdb.sip.id == account) & (astdb.sip.keyword == k)).select().first().data
            argdict['callerid_name'] = astdb(astdb.devices.id == account).select().first().description # get callerid_name
        except:
            log.error('Device with ID=%s has no valid entry in AstDB' % lid)
            continue
        # {'secret': '2004ab', 'dtmfmode': 'rfc2833', 'allow': ''}
        ###################################################################
        # Args to add
        argdict.update({
            'account': account,
            'server': host,
        })
        ###################################################################
        
        ###################################################################\
        # now modify the elastix syntax for GS syntax
        argdict['dtmfmode'] = dtmf_dict[argdict['dtmfmode']] # translate dtmf
        argdict['allow']    = modify_syntax(switch='codec', arg1=argdict['allow']) # translate codecs
        ###################################################################
        
        ###################################################################
        # now translate the argdict to GS-friendly dictionary
        gs_params = {
            'P270': argdict['account'],
            'P47':  argdict['server'],
            'P48':  argdict['server'],
            'P35':  argdict['account'],
            'P36':  argdict['account'],
            'P34':  argdict['secret'],
            'P3' :  argdict['callerid_name'],
            'P57':  argdict['allow'][0],
            'P58':  argdict['allow'][1],
            'P59':  argdict['allow'][2],
            'P60':  argdict['allow'][3],
            'P61':  argdict['allow'][4],
            'P73':  argdict['dtmfmode']
            }
            
        # and add the additional fields from DB
        for row in adddb(adddb.adfields.id > 0).select():
            gs_params[row.key]=row.value
        print gs_params
        
        log.debug('For device ID=%s the following dictionary of parameters was created: %s.' % (lid, json.dumps(gs_params)))
        log.debug('Trying to create provisioning file for device with ID=%s, named %s.' % (lid, macaddr))
        
        ###################################################################
        # creating file
        filepath = os.path.join('applications/provision/configs', '%s.txt' % macaddr)
        with open(filepath, 'w') as f:
            map(lambda x: f.write('%s = %s \n' %(x, gs_params[x])), gs_params.keys())
        log.info('Provisioning template for device with ID=%s created SUCCESSFULLY.' % lid)
        # convert it to gs binary format and write to destination directory (if none write to applications/provision/configs)
        export_path_row = adddb(adddb.configuration.key == 'ExportPath').select().first()
        if export_path_row:
            export_path = export_path_row.value                         #export path needs to end with /
            if export_path[-1] != '/':
                export_path = export_path + '/'
        else:
            export_path = 'applications/provision/configs/'             # export path needs to end with /
        status = subprocess.call(['lua','applications/provision/static/conversion-tool/gs.lua', filepath, export_path])
        if status != 0:
           log.error('Creation of provisioning file for device with ID=%s FAILED.' % lid)
        else:
            log.info('Creation of provisioning file for device with ID=%s SUCCESSFUL.' % lid)
    ###################################################################
    
    return
