#!/usr/bin/lua

-- grandstream.lua macaddress.txt
--     macaddress.txt Configuration with the mac address in the file name

-- The original script, written in perl, can be found at
-- http://www.voip-info.org/wiki/view/Grandstream+Configuration+Tool
-- write_format was taken from http://lua-users.org/wiki/ReadWriteFormat

function write_format(little_endian, format, ...)
  local res = ''
  local values = {...}
  for i=1,#format do
    local size = tonumber(format:sub(i,i))
    local value = values[i]
    local str = ""
    for j=1,size do
      str = str .. string.char(value % 256)
      value = math.floor(value / 256)
    end
    if not little_endian then
      str = string.reverse(str)
    end
    res = res .. str
  end
  return res
end

function getHexWord(mac_address, word_number)
  local start_byte = word_number * 4 - 3
  return tonumber("0x" .. mac_address:sub(start_byte, start_byte + 3))
end

function thresh_config(file_path)
  local file = assert(io.open(file_path, "r"))
  local content = ""
  line = file:read("*line")
  while line do
    line = line:gsub("\n", "")  -- remove newline,
	line = line:gsub("#.*", "") -- comments and
	line = line:gsub("%s+", "") -- spaces
	if line:len() > 0 then
      _, _, p, v = line:find("(%w+)=(.*)")
      line = p .. "=" .. v:gsub("([^%w ])", -- URL encode
               function (c) return string.format("%%%02X", string.byte(c)) end)
      content = content .. line .. "&"
    end
    line = file:read("*line")
  end
  file:close()

  return content
end

function add_trail(content)
  content = content .. "gnkey=0b82"
  if content:len() % 2 ~= 0 then content = content .. "\0" end
  if content:len() % 4 ~= 0 then content = content .. "\0\0" end
  
  return content
end

function get_length(config)
  return 8 + (config:len() / 2) -- Header + Config variables in words
end

function get_checksum(config)
  local sum = 0
  for index = 1, config:len(), 2 do
    local first, second = config:byte(index, index + 1)
    sum = sum + ((first * 256) + second) -- (first << 8) & second
  end
  if sum > 0xFFFF then sum = 0x10000 - sum end -- Ensure a word
  
  return sum
end

config_file = arg[1] -- "000b82xxxxxx.txt" -- File containing all parameters
dest_path   = arg[2] -- "where to write file"
-- mac_string  = config_file:gsub(".txt", "")
mac_string  = config_file:sub(-16, -5)
-- cfg_path = config_file:sub(1,-17)
mac = write_format(false, "222", getHexWord(mac_string, 1),
                                 getHexWord(mac_string, 2),
                                 getHexWord(mac_string, 3))
crlf     = write_format(false, "2", getHexWord("0d0a", 1))
config   = thresh_config(config_file)
config   = add_trail(config)
length   = write_format(false, "4", get_length(config))
checksum = write_format(false, "2", get_checksum(length .. mac .. crlf .. crlf .. config))

file = io.open(dest_path .. "cfg" .. mac_string, "w")
file:write(length)
file:write(checksum)
file:write(mac)
file:write(crlf)
file:write(crlf)
file:write(config)
file:close()
